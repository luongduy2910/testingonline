/**
 * Phân tích yêu cầu bài toán xây dựng bài kiểm tra online 
 * Yêu cầu 1 : 
 * Từ những dữ liệu có trong file dataList.js -> hãy render các câu hỏi ra ngoài giao diện 
 * Lưu ý : 
 * +có 2 dạng câu hỏi là mutiplechoice và fillnblank , mỗi dạng câu hỏi sẽ có cách render khác nhau 
 * + vị trí đứng câu hỏi mà user đang trả lời cũng cần được render ra ngoài giao diện 
 * 
 * Yêu cầu 2 : 
 * Sẽ có một nút "câu tiếp theo" -> khi người dùng nhấn vào nút này sẽ tự động chuyển sang câu hỏi khác
 * Lưu ý : 
 * + Khi qua câu hỏi tiếp theo thì cũng sẽ dựa vào dạng câu hỏi nào mà render ra đúng dạng câu hỏi đó 
 * 
 * Yêu cầu 3: 
 * Tính số câu đúng và số câu hỏi sai mà user đã làm dựa trên thuộc tính được thêm vào trong dữ liệu  
 */


import { checkCorrectandIncorrect, checkFillnBlank, checkMultipleChoice, renderQuestion} from "../controllers/controller.js";
import { listQuestion as question } from "../data/dataList.js";

let listQuestion = question.map(item => {
    return {...item , isCorrect : null} ; 
})

// * Đầu tiên currenIndex sẽ bằng 0 

var currentIndex = 0 ; 

renderQuestion(currentIndex , listQuestion) ; 


let nextQuestion = () => {
    if (listQuestion[currentIndex].questionType == 1) {
        let radioButtonEl = document.querySelectorAll('input[type="radio"]');
        let isChecked = false ;
        for (const radioButton of radioButtonEl) {
            if (radioButton.checked) {
                isChecked = true ;
                break ; 
            }
        }
        if (isChecked) {
            listQuestion[currentIndex].isCorrect = checkMultipleChoice(currentIndex , listQuestion) ; 
            currentIndex++ ; 
            if (currentIndex <= 7) {
                renderQuestion(currentIndex , listQuestion) ;
            }else {
                checkCorrectandIncorrect(listQuestion) ; 
            }
        }else {
            Toastify({

                text: "Bạn chưa điền vào câu trả lời",
                
                duration: 1000
                
                }).showToast();
        }

    }else {
        let inputEl = document.getElementById('fillInput').value ; 
        if (inputEl.length == 0) {
            Toastify({

                text: "Bạn chưa điền vào câu trả lời",
                
                duration: 1000
                
                }).showToast();
        }else {
            listQuestion[currentIndex].isCorrect = checkFillnBlank() ; 
            currentIndex++ ; 
            if (currentIndex <= 7) {
                renderQuestion(currentIndex , listQuestion) ;
            }else {
                checkCorrectandIncorrect(listQuestion) ; 
            }
        }
    }
}

window.nextQuestion = nextQuestion ; 