// TODO: Xây dựng hàm để render ra vị trí đứng mà user đang trả lời câu hỏi 

let renderTitle = (currentIndex , total) => {
    let content = `${currentIndex +1}/${total.length}` ; 
    document.getElementById('currentStep').innerHTML = content ;
}

// TODO: Xây dựng các hàm để render câu hỏi (mutiplechoice và fillnblank)  

export let renderMultipleChoice = (currentIndex , dataList) => {
    renderTitle(currentIndex, dataList) ; 
    let element = dataList[currentIndex] ; 
    let questions = dataList[currentIndex].answers ; 
    var contentHTML = "" ; 
    questions.forEach(item => {
        let contentRadio =
        `
        <div class="form-check">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="multiplechoice" value="${item.id}">
            ${item.content}
            </label>
        </div>
        `
        contentHTML += contentRadio ; 
    })
    document.getElementById('contentQuiz').innerHTML = 
    `<h5>${element.content}</h5>
    ${contentHTML}` ; 
}

export let renderFillnBlank = (currentIndex , dataList) => {
    renderTitle(currentIndex, dataList) ; 
    let element = dataList[currentIndex] ; 
    let inputForm =
    `
    <div class="form-group">
    <input type="text" data-no-answers = "${element.answers[0].content}" class="form-control" id="fillInput" placeholder="Câu trả lời là ...">
    </div>
    `
    document.getElementById('contentQuiz').innerHTML = 
    `<h5>${element.content}</h5>
    ${inputForm}` ; 
}

export let renderQuestion = (currentIndex , dataList) => {
    if (dataList[currentIndex].questionType == 1) {
        renderMultipleChoice(currentIndex, dataList) ; 
    }else {
        renderFillnBlank(currentIndex, dataList) ;
    }
}

// TODO: Xây dựng các hàm để kiểm tra tính đúng sai cho từng loại câu hỏi 

export let checkMultipleChoice = (currentIndex , dataList) => {
    let id = document.querySelector('input[name="multiplechoice"]:checked').value;
    let element = dataList[currentIndex].answers ; 
    var viTri = element.findIndex(item => item.id == id) ; 
    return element[viTri].exact ; 

}

export let checkFillnBlank = () => {
    var {value , dataset} = document.getElementById('fillInput') ; 
    return value == dataset.noAnswers ; 
}

// TODO: Xây dựng hàm để kiểm tra và đếm số câu đúng sai khi user trả lời xong hết câu hỏi 

export let checkCorrectandIncorrect = (dataList) => {
    document.getElementById('endQuiz').classList.remove('d-none') ;  
    document.getElementById('startQuiz').classList.add('d-none') ;  
    let correctAnswers = dataList.filter(item => item.isCorrect == true) ; 
    let inCorrectAnswers = dataList.length - correctAnswers.length ; 
    document.getElementById('correct').innerHTML = correctAnswers.length ; 
    document.getElementById('incorrect').innerHTML = inCorrectAnswers ;
}

